FROM alpine/git as clone
WORKDIR /app
RUN git clone https://shebcornelius:Gossipgirl1!!@gitlab.com/Resolution_Revolution/rr-service.git

FROM adoptopenjdk/maven-openjdk11 as build
WORKDIR /app
COPY --from=clone /app/rr-service /app
RUN mvn package

FROM openjdk:11-jre-slim
WORKDIR /app
COPY --from=build /app/target/rr-service-0.4.0.jar app.jar
ENTRYPOINT ["java", "-jar", "app.jar"]


















