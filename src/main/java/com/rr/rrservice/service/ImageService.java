package com.rr.rrservice.service;

import org.springframework.stereotype.Service;

import com.rr.rrservice.dto.ImageDto;
import com.rr.rrservice.feign.ImageFeignSender;

@Service
public class ImageService {

    private final ImageFeignSender imageSender;

    public ImageService(final ImageFeignSender imageSender) {
        super();
        this.imageSender = imageSender;
    }

    public ImageDto enhanceImage(final ImageDto initialImage) {
        return this.imageSender.sendImage(initialImage);
    }

}
