package com.rr.rrservice.feign;

import org.springframework.stereotype.Service;

import com.rr.rrservice.dto.ImageDto;

@Service
public class ImageFeignSender {

    private final ImageFeignClient feignClient;

    public ImageFeignSender(final ImageFeignClient feignClient) {
        super();
        this.feignClient = feignClient;
    }

    public ImageDto sendImage(final ImageDto initialImage) {
        return this.feignClient.enhance(initialImage);
    }

}
