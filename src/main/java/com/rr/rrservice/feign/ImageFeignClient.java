package com.rr.rrservice.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.rr.rrservice.dto.ImageDto;

@FeignClient(name = "Image", url = "${flaskUrl}")
public interface ImageFeignClient {
    @RequestMapping(method = RequestMethod.POST, value = "/enhance")
    public ImageDto enhance(ImageDto initialImage);
}
