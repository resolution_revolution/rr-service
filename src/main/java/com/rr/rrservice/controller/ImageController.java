package com.rr.rrservice.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.rr.rrservice.dto.ImageDto;
import com.rr.rrservice.service.ImageService;

@Controller
@RequestMapping("/images")
public class ImageController {

    private final ImageService imageService;

    public ImageController(final ImageService imageService) {
        super();
        this.imageService = imageService;
    }

    @PostMapping(path = "/enhance", consumes = "application/json", produces = "application/json")
    public @ResponseBody ResponseEntity<ImageDto> enhanceImage(final ImageDto initialImage) {
        if (this.imageService.enhanceImage(initialImage) != null) {
            return ResponseEntity.ok(this.imageService.enhanceImage(initialImage));
        }
        else {
            return ResponseEntity.badRequest()
                    .body(new ImageDto("null", "null", new byte[] {}));
        }
    }
}
