package com.rr.rrservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class RrServiceApplication {

    public static void main(final String[] args) {
        SpringApplication.run(RrServiceApplication.class, args);
    }

}
