package com.rr.rrservice.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.rr.rrservice.dto.ImageDto;
import com.rr.rrservice.feign.ImageFeignSender;

public class ImageServiceTests {
    private ImageDto initialImage;

    private ImageService imageService;

    @Mock
    private ImageFeignSender feignSender;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        this.imageService = new ImageService(this.feignSender);
        this.initialImage = new ImageDto("pubby", "jpeg", new byte[] { (byte) 0xba });
    }

    @Test
    public void testSendImage() {
        Mockito.when(this.feignSender.sendImage(Mockito.any(ImageDto.class)))
                .thenReturn(this.initialImage);
        assert this.initialImage.equals(this.imageService.enhanceImage(this.initialImage));
    }
}
