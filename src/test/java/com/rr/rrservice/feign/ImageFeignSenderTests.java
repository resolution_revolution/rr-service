package com.rr.rrservice.feign;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.rr.rrservice.dto.ImageDto;

public class ImageFeignSenderTests {

    private ImageDto initialImage;

    private ImageFeignSender feignSender;

    @Mock
    private ImageFeignClient feignClient;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        this.feignSender = new ImageFeignSender(this.feignClient);
        this.initialImage = new ImageDto("pubby", "jpeg", new byte[] { (byte) 0xba });
    }

    @Test
    public void testSendImage() {
        Mockito.when(this.feignClient.enhance(Mockito.any(ImageDto.class)))
                .thenReturn(this.initialImage);
        assert this.initialImage.equals(this.feignSender.sendImage(this.initialImage));
    }
}
