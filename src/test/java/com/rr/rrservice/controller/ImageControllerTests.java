package com.rr.rrservice.controller;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.ResponseEntity;

import com.rr.rrservice.dto.ImageDto;
import com.rr.rrservice.service.ImageService;

public class ImageControllerTests {
    private ImageController imageController;

    private ImageDto dto;

    @Mock
    private ImageService imageService;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        this.imageController = new ImageController(this.imageService);
        this.dto = new ImageDto("x", "y", new byte[] {});
    }

    @Test
    public void testAuthenticateReturnsUser() {
        Mockito.when(this.imageService.enhanceImage(Mockito.any(ImageDto.class)))
                .thenReturn(this.dto);
        assert this.imageController.enhanceImage(this.dto)
                .equals(ResponseEntity.ok(this.dto));
    }

    @Test
    public void testAuthenticateNoUserFound() {
        Mockito.when(this.imageService.enhanceImage(Mockito.any(ImageDto.class)))
                .thenReturn(null);
        assert this.imageController.enhanceImage(this.dto)
                .equals(ResponseEntity.badRequest()
                        .body(new ImageDto("null", "null", new byte[] {})));
    }
}
